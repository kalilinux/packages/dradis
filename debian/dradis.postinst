#!/bin/sh

set -e

setup_dirs_permissions() {
    mkdir -p /var/log/dradis
    for libdir in tmp attachments backup templates; do
        mkdir -p /var/lib/dradis/$libdir
    done

    chown -R dradis:dradis /var/log/dradis /var/lib/dradis /etc/dradis

    # file schema.rb is modified when bin/rails db:migrate. We move the
    # file in /var/lib/dradis (we keep a copy of the file provided by
    # upstream in /usr/lib/dradis/db/schema.rb.initial) and change the
    # rights to allow user dradis to modify it. We create a
    # link between /usr/lib/dradis/db/schema.rb and /var/lib/dradis/..
    if [ ! -e "/var/lib/dradis/schema.rb" ]; then
        cp /usr/lib/dradis/db/schema.rb.initial /var/lib/dradis/schema.rb
        chown dradis:dradis /var/lib/dradis/schema.rb
    fi
    if [ ! -h "/usr/lib/dradis/db/schema.rb" ]; then
        ln -sf /var/lib/dradis/schema.rb /usr/lib/dradis/db/schema.rb
    fi
}

migrate_database() {
    #cd /usr/lib/dradis/ && sudo -u dradis bin/rails db:migrate RAILS_ENV=development
    #if [ ! -e "/var/lib/dradis/development.sqlite3" ]; then
    #    cd /usr/lib/dradis/ && sudo -u dradis bin/rails db:setup
    #fi
    #Replaced by
    /usr/lib/dradis/dradisctl migrate || true
}

initial_setup() {
    adduser --system dradis --group --home /var/lib/dradis
    setup_dirs_permissions
    if [ ! -e "/etc/dradis/database.yml" ]; then
        cp /etc/dradis/database.yml.template /etc/dradis/database.yml
    fi
    /usr/lib/dradis/dradisctl setup || true
}

drop_obsolete_files_from_29() {
    rm -f /etc/init.d/dradis
    rm -f /etc/dradis/initializers/deprecated_tasks.rb
    rm -f /etc/dradis/initializers/deprecated_upload_plugins.rb
    rm -f /etc/dradis/initializers/extjs_json.rb
    rm -f /etc/dradis/initializers/deprecated_configurations.rb
    rm -f /etc/dradis/initializers/secret_token.rb
}

drop_obsolete_files_from_3_0() {
    rm -f /etc/dradis/initializers/z_01_clear_transient_data.rb
    rm -f /etc/dradis/initializers/z_10_export_plugin_categories.rb
}


case $1 in
    configure)
        if [ -z "$2" ]; then
            initial_setup
        else
            setup_dirs_permissions
        fi
        # Special case during upgrades
        if dpkg --compare-versions "$2" lt-nl 3.0.0~rc1; then
            drop_obsolete_files_from_29
        fi
        if dpkg --compare-versions "$2" lt-nl 3.1.0~rc2+git20160609-0kali3; then
            drop_obsolete_files_from_3_0
        fi
        if dpkg --compare-versions "$2" lt-nl 3.6.0-0kali1; then
            # Rerun setup as we switched to RAILS_ENV=development
            /usr/lib/dradis/dradisctl setup || true
        fi
        # Once everything is in place, upgrade the database
        if [ -n "$2" ]; then
            migrate_database
        fi
        ;;
esac


#DEBHELPER#
